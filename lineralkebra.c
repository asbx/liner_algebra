#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct matrix
{
	int len;
	int hoej;
	int *matrix;
};
int *xy_to_len(int len, int hoej,struct matrix *a )
{
	if(len > a->len)
		return NULL;
	if(hoej > a->hoej)
		return NULL;
	return	a->matrix+a->len*hoej+len;
}

void fmetrixf(FILE *strem, struct matrix *a)
{
	for(int i = 0; i < a->len; i++)
	{
		fprintf(strem,"[");
		for(int j = 0; j < a->hoej;j++)
			fprintf(strem,"%d,", *(xy_to_len(i,j,a)));
		fprintf(strem,"]\n");
	}

}

void transpos(struct matrix *a)
{
	struct matrix b = {a->len ,a->hoej, (int *)malloc(sizeof(int)*a->len*a->hoej)};
	memcpy(b.matrix, a->matrix,sizeof(int)*a->len*a->hoej);
	int len = a->len;
	a->len = a->hoej;
	a->hoej = len;
	for(int i = 0; i < a->len; i++)
		for(int j = 0; j < a->hoej; j++)
		{
			*xy_to_len(i,j,a) = *xy_to_len(j,i,&b);
		}
	free(b.matrix);
}

void hen_over(struct matrix *a, int (*fun)(int,int))
{
	for(int i = 0; i < a->len; i++)
		for(int j = 0; j < a->hoej; j++)
			*xy_to_len(i,j,a) = fun(i,j);
}
struct matrix index(int i)
{
	struct matrix a = {i,i,malloc(sizeof(int)*i*i)};
	int dirgonal_tal(int len , int hoej)
	{
		return len == hoej;
	}
	hen_over(&a,dirgonal_tal);
	return a;
}

struct matrix gang(struct matrix a,int i)
{
	int gang(int len, int hoej)
	{
		return ((*xy_to_len(len,hoej,&a))*i);
	}
	hen_over(&a,gang);
	return a;
}
struct matrix dot(struct matrix a, struct matrix b)
{
	struct matrix c;
	if(a.len  != b.hoej)
	{
		fprintf(stderr,"kan ikke gange to matriser hvor 1. matrise len:%d er ikke lige 2. mratrise hoej:%d\n",a.len , b.hoej);
		exit(1);
	}
	c.len = b.len;
	c.hoej = a.hoej;
	c.matrix = malloc(sizeof(int)*c.len*c.hoej);
	int rov(int i , int j)
	{		
		long aklum = 0;
		for(int k = 0; k < a.len; k++)
		{
			aklum += *xy_to_len(k, j, &a) * *xy_to_len(i,k,&b);	
		}
		return aklum;
	}
	hen_over(&c,rov);
	return c;
}
int determinant_2x2(struct matrix a)
{
	if(a.len != 2 || a.hoej != 2)
	{
		fmetrixf(stderr, &a);
		fprintf(stderr,"determinant på en ikke kvardardisk kan ikke lade sig gøre\n");
		exit(1);
	}
	return a.matrix[0]*a.matrix[3]-a.matrix[1]*a.matrix[2];
}

int determinat(struct matrix a)
{
	//fmetrixf(stdout,&a);
	if(a.len != a.hoej)
	{
		fmetrixf(stderr, &a);
		fprintf(stderr,"determinant 2x2 på en ikke 2x2 matrix kan ikke lade sig gøre\n");
		exit(1);
	}
	if(a.len == 2)
		return determinant_2x2(a);
	int ekum = 0;
	struct matrix b = {a.len-1, a.hoej-1, malloc(sizeof(int)*(a.len-1)*(a.hoej-1))};
	for(int i = 0; i < a.len; i++)
	{
		if(a.matrix[i] != 0)
		{
			//printf("\n%d\n",*xy_to_len(0,i,&a));
			for(int j = 1; j < a.len;j++ )
			{
				int bk = 0;
				for(int k = 0; k < a.hoej;k++)
				{
					if(k != i)
					{
						*xy_to_len(j-1,bk,&b) = *xy_to_len(j,k,&a);
						bk++;
						if(bk > (a.len-1))
							bk = 0;
					}
				}
			
			}
			ekum += (i & 1 ? -1 : 1)*determinat(b)**xy_to_len(0,i,&a);
		}
	}
	free(b.matrix);
	return ekum;
}
double *løs(struct matrix a,struct matrix vectoer)
{
	if(vectoer.len == 1)
	{
		printf("\n");
		fmetrixf(stderr, &vectoer);
		fprintf(stderr,"vectoeren har ikke dimisjoner 1xn\n");
		exit(1);
	}
	if(a.len != vectoer.len)
	{
		fmetrixf(stderr, &a);
		fmetrixf(stderr, &vectoer);
		fprintf(stderr,"\n");
		exit(1);
	}
	fmetrixf(stdout,&a);
	double *determinanter = malloc(sizeof(int)*a.len); 
	double dele = determinat(a);

	printf("dert:%d\n\n",determinat(a));
	fmetrixf(stdout,&vectoer);
	printf("\n\n");
	
	struct matrix b = {a.len, a.hoej, malloc(sizeof(int)*a.len*a.hoej)};
	for(int i = 0; i < vectoer.len; i++)
	{
		printf("-----\n");
		int vector_byt(int len, int hoej)
		{
			return i==hoej ? *xy_to_len(len,0,&vectoer): *xy_to_len(len,hoej,&a) ;
		}
		hen_over(&b,vector_byt);
		fmetrixf(stdout,&b);
		printf("dert:%d\n",determinat(b));
		determinanter[i] = determinat(b)/dele;
	}
	return determinanter;

}
int main()
{
	int len = 3;
	int hoej = 4;
	struct matrix a = {len ,hoej, (int *)malloc(sizeof(int)*len*hoej)};
	for(int i = 0; i < len*hoej;i++)
		*(a.matrix+i) = i;
	fmetrixf(stdout, &a);
	transpos(&a);
	printf("\n\n");
	fmetrixf(stdout, &a);
	struct matrix b = index(10);
	fmetrixf(stdout, &b);
	printf("\n");
	struct matrix c = dot(b,gang(b,2));
	fmetrixf(stdout, &c);
	printf("determinat %d\n",determinat(c));
	printf("\n");
	struct matrix d = {1 ,2, (int *)malloc(sizeof(int)*3)};
	d.matrix[0] = 1;
	d.matrix[1] = 2;
	fmetrixf(stdout, &d);
	printf("\n");
	struct matrix e = {2 ,1, (int *)malloc(sizeof(int)*3)};
	e.matrix[0] = 4;
	e.matrix[1] = 5;
	fmetrixf(stdout, &e);
	printf("\n");
	struct matrix f = dot(d,e);
	//f.matrix[0]=-2;
	fmetrixf(stdout, &f);
	printf("determinat %d\n",determinant_2x2(f));
	printf("\n");
	struct matrix g = dot(e,d);
	fmetrixf(stdout, &g);
	printf("\n");
	int hhh[9] = { 
		1,4,-1,
		2,-9,3,
		1,2,1};
	struct matrix h = {3 ,3,hhh };
	fmetrixf(stdout,&h);
	int hh[3] = {-10,-11,-12};
	struct matrix vec = {3 ,1,hh };
	printf("hej\n");
	double *kalle = løs(h,vec);
	printf("hej\n");
	for(int i = 0; i < 3; i++)
		printf("løsninger[%d] %f\n",i,kalle[i]);
	printf("\n");
	printf("dertimant:%d\n",determinat(h));
	return 0;
}
